package Projeto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class conexao {
	private String url;
	private String usuario;
	private String senha;
	private Connection con;
	
	conexao(){
		url = "jdbc:postgresql://localhost:5432/postgres";
		usuario = "postgres";
		senha = "123456";
		try {
			Class.forName("org.postgresql.Driver");
			setCon(DriverManager.getConnection(url,usuario,senha));
			System.out.println("Conexo Estabelecida com o Banco Postgres");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int executaSQL(String sql){
		try {
			Statement stm = con.createStatement();
			int res = stm.executeUpdate(sql);
			con.close();
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		
	}
	
	public ResultSet executaBusca(String sql){
		try {
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery(sql);
			con.close();
			return rs;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}

	public ResultSet executaBusca(String sql) {
		// TODO Auto-generated method stub
		return null;
	}
}
