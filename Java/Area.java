package Projeto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.StringJoiner;

public class Area{

    public static void main(String[] args) throws SQLException {

        Connection conexao = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres", "123456");

        exibirTabela(conexao, "area_imovel_3508504");

        conexao.close();
    }

    public static void exibirTabela(Connection conexao, String area_imovel_3508504) throws SQLException {
        ResultSet rs = conexao.prepareStatement("SELECT * FROM " + area_imovel_3508504).executeQuery();

        ResultSetMetaData metaData = rs.getMetaData();
        int numeroDeColunas = metaData.getColumnCount();

        while (rs.next()) {
            StringJoiner joiner = new StringJoiner(", ", "", "");
            for (int coluna = 1; coluna <= numeroDeColunas; coluna++) {
                String nomeDaColuna = metaData.getColumnName(coluna);
                joiner.add(nomeDaColuna + "=" + rs.getObject(coluna));
            }
            System.out.println(joiner.toString());
        }

        rs.close();
    }
}