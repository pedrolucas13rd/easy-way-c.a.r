package Projeto;

import java.sql.ResultSet;

public class Tela {
	private static conexao con;

	public static void main(String[] args) {
		setCon(new conexao());
		String sql = " Select * from reserva_legal_3508504";
		ResultSet rs = con.executaBusca(sql);
		
		try {
			while(rs.next()){
				int id = rs.getInt("idf");
				String nom_tema = rs.getString("nom_tema");
				String num_area = rs.getString("num_area");
		
				System.out.println(id+ " - "+nom_tema+" - "+num_area);
			
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
			
	}

	public static conexao getCon() {
		return con;
	}

	public static void setCon(conexao con) {
		Tela.con = con;
	}
}
