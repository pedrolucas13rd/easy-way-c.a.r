****
**PROJETO INTEGRADOR - 2º SEMESTRE DO CURSO DE BANCO DE DADOS**


****
****
****
**Grupo 3 - EasyWay**
_________________________________________________________________________

**Scrum Master**: Pedro Lucas dos Santos Rodrigues 

**Product Owner**: Isaque Costa Beirao

**Desenvolvedor**: Adriano Ribeiro Martins

**Desenvolvedor**: Leonardo Souza Ferrianci

**Desenvolvedor**: Marcos Francisco da Silva

**Desenvolvedor**: Orlando Pereira de Seixas Neto




______________________________________________________________________________________________________________________________
****    
****



**PROBLEMATIZAÇÃO**

****
Requisitado a solução de integração de um banco de dados para readequação do sistema do Governo Federal "CAR"(Cadastro Ambiental Rural), no qual as informações mais relevantes como Área do imóvel, Área de Preservação Permanente e Reserva Legal sejam acessados com mais facilidade.




![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/Capturar4.JPG)

______________________________________________________________________________________________________________________________
****
****
**O que é o CAR?**
****

Criado pela Lei nº 12.651/2012, no âmbito do Sistema Nacional de Informação sobre Meio Ambiente - SINIMA, e regulamentado pela Instrução Normativa MMA nº 2, de 5 de maio de 2014, o Cadastro Ambiental Rural – CAR é um registro público eletrônico de âmbito nacional, obrigatório para todos os imóveis rurais, com a finalidade de integrar as informações ambientais das propriedades e posses rurais referentes às Áreas de Preservação Permanente - APP, de uso restrito, de Reserva Legal, de remanescentes de florestas e demais formas de vegetação nativa, e das áreas consolidadas, compondo base de dados para controle, monitoramento, planejamento ambiental e econômico e combate ao desmatamento.

A inscrição no CAR é o primeiro passo para obtenção da regularidade ambiental do imóvel, e contempla: dados do proprietário, possuidor rural ou responsável direto pelo imóvel rural; dados sobre os documentos de comprovação de propriedade e ou posse; e informações georreferenciadas do perímetro do imóvel, das áreas de interesse social e das áreas de utilidade pública, com a informação da localização dos remanescentes de vegetação nativa, das Áreas de Preservação Permanente, das áreas de Uso Restrito, das áreas consolidadas e das Reservas Legais.



![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/okkk.jpg)


________________________________________________________________________________________________________________________________
****
****
**Primeira Reunião com o Cliente "Visiona"**
****


Na primeira reunião, nosso PO Fernando conseguiu extrair informações importantes para a entrega da Primeira Sprint, bem como elencar alguns pontos de desenvolvimento da aplicação solicitada, analisando quais são os requisitos que o cliente esperar receber nessa entrega.

Dentre os pontos relevantes citados pelo Cliente:

- Um Sistema que armazene em banco as informações principais de área do imóvel, Área de Preservação Permanente e reserva Legal;

- Necessário BOT para chamar os dados da página web e inserí-los no sistema de banco, que formos utilizar(Utilizando-se de machine learning e deep learning);

- Os resultados do banco tem que ser atualizados a cada 15 e 30 dias, fazendo comparação dos dados em banco, e caso haja mudança, substituí-los;

- Informado pelos CLIENTES que o sistema de POSTGRES possui um banco que agrega valor chamado POSTGIS, o qual poderia ser utilizado na aplicação.




![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/reuniao_teams_5_menor.jpg)

****
****

__________________________________________________________________________________________________________________
****
****

**SPRINT - 1**
****

Nessa Sprint estamos entregando uma análise das soluções que serão necessárias para o desenvolvimento do banco de dados e da sua integração com a Linguagem de Programação que utilizaremos nas Sprints posteriores. Estamos também efetuando a coleta dos dados no sistema SICAR, onde será agrupado os mesmos por Estado para padronização do novo Banco de dados. Nas próximas entregas, carregaremos o nosso banco de dados com os ShapeFiles já existentes no SICAR e faremos a solução para a busca de dados padronizados em um único cadastro.
_________________________________________________________________________________________________________________________________
****
****
**Plantão de dúvidas com cliente da “Visiona”:**
________________________________________

Em uma reunião, nosso PO Isaque buscou aspectos bem promissores para esta entrega, na qual houve pontos críticos para o desenvolvimento dessa sprint.
Dentre os pontos relevantes citados pelo Cliente, para se obter uma melhor interface devemos visar:
- Uma interface de maior auxílio ao usuário, na qual realizaria carga no banco de dados.
Além disso, para melhorar o fluxo do processo temos duas etapas que são vistas como importantes:

1 - Salvar os dados no banco de dados por cidade, de acordo com a classes de uso (APP, RL, Área do Imóvel etc.);

2 - Atualizar essa base em função das mudanças obtidas nos dados baixados em um T0 para um T1.

Com essas informações, demos continuação aos trabalhos no projeto para entrega da Sprint.

****
****
**SPRINT - 2**

- Ao início da segunda sprint, houve uma seleção das cidades que serão usadas como base no projeto, logo em seguida foi realizado a classificação dos dados baixados em Shapefile.
- Inicialmente seria utilizado POSTGIS como banco para esses dados e QGIS como sistema de visualização geográfica, porém ao ser realizado uma verificação com o cliente foi utilizado apenas o POSTGIS, após isso houve a importação de dados para o Banco de Dados POSTGIS via Postgis Shapefile and dbf Loader, uma ferramenta do próprio sistema.
- Foi realizado a integração entre o JAVA e o POSTGIS, em seguida a exibição dos dados do POSTGIS dentro da ferramenta JAVA, na aba Console, com isso iniciou-se a codificação da interface gráfica para expor e interagir com esses tais dados.

****
****
**Imagens da integração entre o JAVA e o POSTGIS**

![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/Area_Im%C3%B3vel.PNG)


______________________________________________________________________________________________________________________
****
****
****

![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/MG_reserva_legal.PNG)


______________________________________________________________________________________________________________________

****
****
****
****

![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/reserva_legal__SP_OK.PNG)


______________________________________________________________________________________________________________________
****
****
****
**Tela 1 - Seleção de Estado**

 ![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/Tela_Inicial.png)


______________________________________________________________________________________________________________________
****
****
****
**Tela 2 - Seleção da Cidade**

  ![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/Tela4.png)


______________________________________________________________________________________________________________________
****
****
****
**Tela 3 - Exposição dos Dados**

  ![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/Tela2.png)


______________________________________________________________________________________________________________________
****
****
****
**Tela 4 - Alteração dos Dados**

  ![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/Tela3.png)


______________________________________________________________________________________________________________________
****
****
****
**Sprint – 3 (Planejamento)**

**•	Método de rotina de atualização do banco de dados.**

**•	Gerar o Funcionamento da interface gráfica.**

**•	Gerar um programa no Java para a alteração de dados.**

**•	Melhoria na eficiência da interação com interface.**
________________________________________
**Tecnologias utilizadas:**


  ![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/soft.png)


______________________________________________________________________________________________________________________
****
****
****

  ![Terminal](https://gitlab.com/pedrolucas13rd/easy-way-c.a.r/-/raw/master/Docs/images.png)


______________________________________________________________________________________________________________________














